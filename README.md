Actual docs
[here](https://g-braeunlich.gitlab.io/egui_extras/widgets/index.html).

The core of the docs are generated from
[widgets/src/lib.rs](./widgets/src/lib.rs) and the used the wasm
generated from this lib: [examples/src/lib.rs](./examples/src/lib.rs).

Here is how the whole thing is built: [.gitlab-ci.yml](./.gitlab-ci.yml).
