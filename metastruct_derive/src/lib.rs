extern crate proc_macro;

use crate::proc_macro::TokenStream;
use quote::quote;

#[proc_macro_derive(MetaStruct)]
pub fn config_macro_derive(input: TokenStream) -> TokenStream {
    let syn::DeriveInput { ident, data, .. } = syn::parse_macro_input!(input as syn::DeriveInput);
    let fields: Vec<_> = match data {
        syn::Data::Struct(s) => match s.fields {
            syn::Fields::Named(syn::FieldsNamed { named, .. }) => named
                .iter()
                .filter_map(
                    |f| match (extract_doc(f), extract_types(&f.ty, &["f64", "usize"])) {
                        (Some(doc), Some(ty)) => Some((f.ident.clone(), doc, ty)),
                        _ => None,
                    },
                )
                .collect(),
            _ => {
                panic!("Must be an struct with named fields!");
            }
        },
        _ => panic!("Must be a struct!"),
    };
    let quoted_fields = fields.into_iter().map(|(ident, doc, ty)| {
        let (sym, label, unit) = parse_doc(&doc);
        let unit = unit.unwrap_or_default();
        let sym = sym.unwrap_or_default();
        let extract_type = quote::format_ident!("{}{}", ty[0..1].to_uppercase(), &ty[1..]);
        quote! {
            metastruct::MetaField::<Self> {ident: stringify!(#ident), symbol: #sym, label: #label, unit: #unit, extract_mut: metastruct::Extractor::<Self>::#extract_type(|obj| &mut obj.#ident)}
        }
    });
    let output = quote! {
        impl metastruct::MetaStruct for #ident {
            const META_FIELDS: metastruct::MetaFields<Self> = &[
                #(#quoted_fields),*
            ];
        }
    };
    output.into()
}

fn extract_types(ty: &syn::Type, filter_types: &[&'static str]) -> Option<&'static str> {
    if let syn::Type::Path(p) = ty {
        if p.path.segments.len() != 1 {
            return None;
        }
        if let Some(seg) = p.path.segments.first() {
            let repr = &seg.ident.to_string();
            for t in filter_types {
                if repr == t {
                    return Some(t);
                }
            }
        }
    }
    None
}

fn extract_doc(field: &syn::Field) -> Option<String> {
    let docs = field
        .attrs
        .iter()
        .filter_map(|attr| attr.parse_meta().ok())
        .filter_map(|meta| match meta {
            syn::Meta::NameValue(syn::MetaNameValue {
                path,
                lit: syn::Lit::Str(doc),
                ..
            }) if path.get_ident().is_some() && *path.get_ident().unwrap() == "doc" => {
                Some(doc.value())
            }
            _ => None,
        })
        .collect::<Vec<_>>();
    if !docs.is_empty() {
        Some(docs.join(""))
    } else {
        None
    }
}

/** Parses a doc string in the format
 *  sym: label [unit]
 *  returns the 3 parts sym, label, unit
 */
fn parse_doc(doc: &str) -> (Option<&str>, &str, Option<&str>) {
    let doc = doc.trim();
    let (sym, doc) = split_once(doc, ':')
        .and_then(|(s, d)| {
            let s = s.trim();
            if s.contains(' ') {
                None
            } else {
                Some((Some(s), d.trim()))
            }
        })
        .unwrap_or((None, doc));
    let (doc, unit) = split_once(doc, '[')
        .and_then(|(d, u)| {
            if let Some(u_trimmed) = u.trim().strip_suffix(']') {
                Some((d.trim(), Some(u_trimmed)))
            } else {
                None
            }
        })
        .unwrap_or((doc, None));
    (sym, doc, unit)
}

/// Own implementation of the unstable split_once std::str function
fn split_once(s: &str, separator: char) -> Option<(&str, &str)> {
    if let Some(pos) = s.find(separator) {
        let (a, b) = s.split_at(pos);
        Some((a, &b[1..]))
    } else {
        None
    }
}
