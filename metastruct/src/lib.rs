#[allow(unused_imports)]
#[macro_use]
extern crate metastruct_derive;
#[doc(hidden)]
pub use metastruct_derive::*;

pub struct MetaField<T> {
    pub ident: &'static str,
    pub symbol: &'static str,
    pub label: &'static str,
    pub unit: &'static str,
    pub extract_mut: Extractor<T>,
}

pub enum Extractor<T> {
    F64(fn(&mut T) -> &mut f64),
    Usize(fn(&mut T) -> &mut usize),
}

impl<T> Into<Extractor<T>> for fn(&mut T) -> &mut f64 {
    fn into(self) -> Extractor<T> {
        Extractor::F64(self)
    }
}
impl<T> Into<Extractor<T>> for fn(&mut T) -> &mut usize {
    fn into(self) -> Extractor<T> {
        Extractor::Usize(self)
    }
}

pub type MetaFields<T> = &'static [MetaField<T>];

pub trait MetaStruct: Sized + 'static {
    const META_FIELDS: MetaFields<Self>;
}
