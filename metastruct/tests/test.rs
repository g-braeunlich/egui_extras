use metastruct::MetaStruct;

#[derive(MetaStruct)]
struct X {
    /// doc xxx
    pub x: f64,
    pub y: u32,
    pub z: f64,
    /// A: doc
    pub a: f64,
    /// doc [unit]
    pub b: f64,
    /// C: doc [unit]
    pub c: f64,
}

#[test]
fn test_derive_meta_field() {
    let mut x = X {
        x: 0.1,
        y: 1,
        z: 4.0,
        a: 1.,
        b: 1.,
        c: 1.,
    };
    match X::META_FIELDS[0].extract_mut {
        metastruct::Extractor::F64(extract_mut) => {
            assert!(f64::abs(*extract_mut(&mut x) - 0.1) < f64::EPSILON)
        }
        _ => panic!("Wrong type"),
    };
    let metastruct::MetaField {
        ident,
        symbol,
        label,
        unit,
        ..
    } = X::META_FIELDS[1];
    assert_eq!(ident, "a");
    assert_eq!(symbol, "A");
    assert_eq!(label, "doc");
    assert_eq!(unit, "");
    let metastruct::MetaField {
        ident,
        symbol,
        label,
        unit,
        ..
    } = X::META_FIELDS[2];
    assert_eq!(ident, "b");
    assert_eq!(symbol, "");
    assert_eq!(label, "doc");
    assert_eq!(unit, "unit");
    let metastruct::MetaField {
        ident,
        symbol,
        label,
        unit,
        ..
    } = X::META_FIELDS[3];
    assert_eq!(ident, "c");
    assert_eq!(symbol, "C");
    assert_eq!(label, "doc");
    assert_eq!(unit, "unit");
}

/*
#[test]
fn test_parse_doc() {
    let doc = "simple doc";
    assert_eq!(proc_macros::parse_doc(doc), (None, String::from(doc), None));
    let sym_doc = "X: simple doc";
    assert_eq!(
        proc_macros::parse_doc(sym_doc),
        (Some(String::from("X")), String::from(doc), None)
    );
    println!("{}", X::META_FIELDS[0].ident);
}
*/
