#![forbid(unsafe_code)]
#![cfg_attr(not(debug_assertions), deny(warnings))]
#![warn(clippy::all)]

use eframe::{egui, epi};
use metastruct::MetaStruct;
use widgets::tabularize::Tabularize;

#[derive(Default, MetaStruct)]
struct Item {
    /// a: Field A [kg/m^3]
    pub a: f64,
    /// b: Field B [kg]
    pub b: f64,
    /// c: Field C [m]
    pub c: f64,
}

#[derive(Default)]
pub struct App {
    tab: Item,
}

impl epi::App for App {
    fn name(&self) -> &str {
        "Tabularize"
    }
    fn max_size_points(&self) -> egui::Vec2 {
        egui::Vec2 { x: 200., y: 100. }
    }

    fn update(&mut self, ctx: &egui::CtxRef, _frame: &mut epi::Frame<'_>) {
        egui::CentralPanel::default().show(ctx, |ui| {
            self.tab.tabularize("id_example_tab", ui);
        });
    }
}
