#![forbid(unsafe_code)]
#![cfg_attr(not(debug_assertions), deny(warnings))]
#![warn(clippy::all)]

use widgets::error_bar::show_error_bar;

use eframe::{egui, epi};

pub struct App {
    pub error_msg: Option<String>,
}

impl epi::App for App {
    fn name(&self) -> &str {
        "Error Bar"
    }
    fn max_size_points(&self) -> egui::Vec2 {
        egui::Vec2 { x: 400., y: 50. }
    }
    fn update(&mut self, ctx: &egui::CtxRef, _frame: &mut epi::Frame<'_>) {
        egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
            show_error_bar(&mut self.error_msg, ui);
        });
        if self.error_msg.is_none() {
            egui::CentralPanel::default().show(ctx, |ui| {
                if ui.button("Press to let 💩 happen again!").clicked() {
                    self.error_msg = Some("💩 happens!".into());
                }
            });
        }
    }
}

impl App {
    pub fn new() -> Self {
        Self {
            error_msg: Some("💩 happens!".into()),
        }
    }
}
