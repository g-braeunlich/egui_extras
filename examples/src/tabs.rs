#![forbid(unsafe_code)]
#![warn(clippy::all)]

use eframe::{egui, epi};
use widgets::tabs::{ShowTabs, Tab};

struct State {
    pub vec: Vec<f32>,
}
impl Default for State {
    fn default() -> Self {
        Self {
            vec: vec![0., 1., 2.],
        }
    }
}

pub struct App {
    state: State,
    selected_tab: usize,
}

impl epi::App for App {
    fn name(&self) -> &str {
        "Tabs"
    }
    fn max_size_points(&self) -> egui::Vec2 {
        egui::Vec2 { x: 400., y: 100. }
    }

    fn update(&mut self, ctx: &egui::CtxRef, _frame: &mut epi::Frame<'_>) {
        egui::CentralPanel::default().show(ctx, |ui| {
            TABS.show(&mut self.selected_tab, &mut self.state, ui);
        });
    }
}

/// This could be automated with a (proc) macro:
const TABS: &[Tab<State>] = &[
    ("Panel A", a_panel),
    ("Panel B", b_panel),
    ("Panel C", c_panel),
];

/// Panel A
fn a_panel(state: &mut State, ui: &mut egui::Ui) {
    ui.heading("Contents of panel A");
    ui.drag_angle(&mut state.vec[0]);
}

/// Panel B
fn b_panel(state: &mut State, ui: &mut egui::Ui) {
    ui.heading("Contents of panel B");
    ui.drag_angle(&mut state.vec[1]);
}

/// Panel C
fn c_panel(state: &mut State, ui: &mut egui::Ui) {
    ui.heading("Contents of panel C");
    ui.drag_angle(&mut state.vec[2]);
}

impl App {
    pub fn new() -> Self {
        Self {
            selected_tab: 0,
            state: Default::default(),
        }
    }
}
