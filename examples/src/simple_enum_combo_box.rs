#![forbid(unsafe_code)]
#![cfg_attr(not(debug_assertions), deny(warnings))]
#![warn(clippy::all)]

use widgets::{enum_combo_box::show_simple_enum_combo_box, setup_simple_enum};

use eframe::{egui, epi};

#[derive(Clone, Copy, PartialEq)]
enum SimpleEnum {
    A,
    B,
    C,
}

setup_simple_enum!(SimpleEnum{ A => "Option A", B => "Option B", C => "Option B" });

pub struct App {
    choice: SimpleEnum,
}

impl epi::App for App {
    fn name(&self) -> &str {
        "Simple Enum Combo Box"
    }
    fn max_size_points(&self) -> egui::Vec2 {
        egui::Vec2 { x: 400., y: 50. }
    }

    fn update(&mut self, ctx: &egui::CtxRef, _frame: &mut epi::Frame<'_>) {
        egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
            show_simple_enum_combo_box(&mut self.choice, "Choose some option", ui);
        });
    }
}

impl App {
    pub fn new() -> Self {
        Self {
            choice: SimpleEnum::A,
        }
    }
}
