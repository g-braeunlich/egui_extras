#![forbid(unsafe_code)]
#![cfg_attr(not(debug_assertions), deny(warnings))]
#![warn(clippy::all)]

use metastruct::MetaStruct;
use widgets::widget_array::TabularWidgetArray;

use eframe::{egui, epi};

#[derive(Default, MetaStruct)]
struct Item {
    /// a: Field A [kg/m^3]
    pub a: f64,
    /// b: Field B [kg]
    pub b: f64,
    /// c: Field C [m]
    pub c: f64,
}

#[derive(Default)]
pub struct App {
    arr: Vec<Item>,
}

impl epi::App for App {
    fn name(&self) -> &str {
        "Tabular Widget Array"
    }
    fn max_size_points(&self) -> egui::Vec2 {
        egui::Vec2 { x: 500., y: 200. }
    }

    fn update(&mut self, ctx: &egui::CtxRef, _frame: &mut epi::Frame<'_>) {
        egui::CentralPanel::default().show(ctx, |ui| {
            TabularWidgetArray {
                id: "table",
                fields: Item::META_FIELDS,
            }
            .show(&mut self.arr, ui);
        });
    }
}
