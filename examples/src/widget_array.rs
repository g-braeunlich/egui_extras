#![forbid(unsafe_code)]
#![cfg_attr(not(debug_assertions), deny(warnings))]
#![warn(clippy::all)]

use eframe::{egui, epi};
use widgets::widget_array::WidgetArray;

#[derive(Default)]
struct Item {
    pub a: u32,
    pub b: bool,
}

#[derive(Default)]
pub struct App {
    arr: Vec<Item>,
    selected_id: usize,
}

impl epi::App for App {
    fn name(&self) -> &str {
        "Widget Array"
    }
    fn max_size_points(&self) -> egui::Vec2 {
        egui::Vec2 { x: 400., y: 100. }
    }

    fn update(&mut self, ctx: &egui::CtxRef, _frame: &mut epi::Frame<'_>) {
        let mut wa = WidgetArray::new(|item: &mut Item, ui: &mut egui::Ui| {
            ui.label("Some numeric field:");
            ui.add(egui::widgets::DragValue::new(&mut item.a));
            ui.add(egui::widgets::Checkbox::new(&mut item.b, "Boolean field"));
        });
        egui::CentralPanel::default().show(ctx, |ui| {
            wa.show(&mut self.selected_id, &mut self.arr, ui);
        });
    }
}
