#![forbid(unsafe_code)]
#![cfg_attr(not(debug_assertions), deny(warnings))]
#![warn(clippy::all)]

use widgets::file_dialog::{show_file_dialog, FileDialogMode, FileDialogState};

use eframe::{egui, epi};

#[derive(Default)]
pub struct App {
    pub file_dialog_state: FileDialogState,
    pub path: std::path::PathBuf,
}

impl epi::App for App {
    fn name(&self) -> &str {
        "File Dialog"
    }
    fn max_size_points(&self) -> egui::Vec2 {
        egui::Vec2 { x: 600., y: 150. }
    }

    fn update(&mut self, ctx: &egui::CtxRef, _frame: &mut epi::Frame<'_>) {
        egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
            egui::menu::bar(ui, |ui| {
                // load
                if ui.button("🗁").clicked() {
                    self.file_dialog_state.mode = FileDialogMode::Read;
                    self.file_dialog_state.open = true;
                }
                // save
                if ui.button("💾").clicked() {
                    self.file_dialog_state.mode = FileDialogMode::Write;
                    self.file_dialog_state.open = true;
                }
            });
            if let Some(path) = show_file_dialog(&mut self.file_dialog_state, ui) {
                self.path = path;
            }
        });
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.heading("Selected file:");
            ui.label(format!("{}", self.file_dialog_state.path.to_string_lossy()));
        });
    }
}
