#![forbid(unsafe_code)]
#![cfg_attr(not(debug_assertions), deny(warnings))]
#![warn(clippy::all, rust_2018_idioms)]

pub mod enum_combo_box;
pub mod error_bar;
pub mod file_dialog;
pub mod simple_enum_combo_box;
pub mod tabs;
pub mod tabular_widget_array;
pub mod tabularize;
pub mod widget_array;

#[cfg(target_arch = "wasm32")]
use eframe::wasm_bindgen::{self, prelude::*};

#[cfg(target_arch = "wasm32")]
#[wasm_bindgen]
pub fn start(canvas_id: &str) -> Result<(), wasm_bindgen::JsValue> {
    use eframe::epi;
    let app: Box<dyn epi::App> = match canvas_id.strip_prefix("cvs_").unwrap_or("") {
        "enum_combo_box" => Box::new(enum_combo_box::App::new()),
        "error_bar" => Box::new(error_bar::App::new()),
        "file_dialog" => Box::new(file_dialog::App::default()),
        "simple_enum_combo_box" => Box::new(simple_enum_combo_box::App::new()),
        "tabs" => Box::new(tabs::App::new()),
        "tabularize" => Box::new(tabularize::App::default()),
        "tabular_widget_array" => Box::new(tabular_widget_array::App::default()),
        "widget_array" => Box::new(widget_array::App::default()),
        _ => {
            panic!("invalid canvas id");
        }
    };
    eframe::start_web(canvas_id, app)
}
