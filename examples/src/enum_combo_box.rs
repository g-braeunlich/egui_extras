#![forbid(unsafe_code)]
#![cfg_attr(not(debug_assertions), deny(warnings))]
#![warn(clippy::all)]

use widgets::{
    enum_combo_box::{show_enum_combo_box, EnumMemory},
    setup_enum,
};

use eframe::{egui, epi};

#[derive(Clone)]
enum ComplexEnum {
    A(f64),
    B(bool),
    C(()),
}
setup_enum!(ComplexEnum, ComplexEnumDiscriminant { A => "Option A", B => "Option B", C => "Option C"});

pub struct App {
    choice: ComplexEnum,
    enum_memory: EnumMemory<ComplexEnum>,
}

impl epi::App for App {
    fn name(&self) -> &str {
        "Enum Combo Box"
    }
    fn max_size_points(&self) -> egui::Vec2 {
        egui::Vec2 { x: 400., y: 100. }
    }

    fn update(&mut self, ctx: &egui::CtxRef, _frame: &mut epi::Frame<'_>) {
        egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
            show_enum_combo_box(
                &mut self.choice,
                "Pick an option",
                &mut self.enum_memory,
                ui,
            );
            match &mut self.choice {
                ComplexEnum::A(x) => {
                    ui.label("A's value:");
                    ui.add(egui::widgets::DragValue::new(x));
                }
                ComplexEnum::B(x) => {
                    ui.add(egui::widgets::Checkbox::new(x, "B's value"));
                }
                _ => {}
            }
        });
    }
}

impl App {
    pub fn new() -> Self {
        Self {
            choice: ComplexEnum::A(0.),
            enum_memory: Default::default(),
        }
    }
}
