use eframe::egui;
pub const CLR_ERR: egui::Color32 = egui::Color32::from_rgb(243, 89, 47);
pub const CLR_OK: egui::Color32 = egui::Color32::GREEN;

#[derive(Default)]
pub struct PathBufFieldState {
    pub buffer: String,
    pub valid: bool,
    pub file_exists: bool,
    pub dir_part_exists: bool,
}

pub fn show_path_buf_field(
    path: &mut std::path::PathBuf,
    state: &mut PathBufFieldState,
    validator: fn(&std::path::PathBuf) -> bool,
    ui: &mut egui::Ui,
) {
    let buffer = &mut state.buffer;
    let mut lost_kb_focus = false;
    let mut has_kb_focus = false;
    let color = if state.valid { CLR_OK } else { CLR_ERR };
    egui::containers::Frame::group(&egui::style::Style::default())
        .stroke(egui::Stroke::new(1., color))
        .show(ui, |ui| {
            let response = ui.text_edit_singleline(buffer);
            lost_kb_focus = response.lost_focus();
            has_kb_focus = response.has_focus();
        });

    let mut validate = false;
    if lost_kb_focus {
        *path = std::path::PathBuf::from(&buffer);
        validate = true;
    } else if !has_kb_focus && path.as_os_str() != buffer as &str {
        *buffer = path.to_string_lossy().to_string();
        validate = true;
    }
    if validate {
        state.valid = validator(path);
        state.file_exists = file_exists(path);
        state.dir_part_exists = dir_part_exists(path);
    }
}

pub fn file_exists(path: &std::path::PathBuf) -> bool {
    path.exists()
}

pub fn dir_part_exists(path: &std::path::PathBuf) -> bool {
    path.parent().map(|p| p.exists()).unwrap_or(true)
}
