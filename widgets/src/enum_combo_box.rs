use eframe::egui;

pub fn show_simple_enum_combo_box<E: VariantList + IntoLabel + PartialEq>(
    variant: &mut E,
    label: impl Into<egui::Label>,
    ui: &mut egui::Ui,
) {
    let variant_str = (*variant).into_label();
    egui::ComboBox::from_label(label)
        .selected_text(variant_str)
        .show_ui(ui, |ui| {
            for v in E::VARIANTS {
                ui.selectable_value(variant, *v, (*v).into_label());
            }
        });
}

pub fn show_enum_combo_box<E: Discriminant + Clone>(
    variant: &mut E,
    label: impl Into<egui::Label>,
    state: &mut EnumMemory<E>,
    ui: &mut egui::Ui,
) where
    E::DiscriminantType: PartialEq + IntoLabel + VariantList + Into<E>,
{
    let original_discriminant = variant.discriminant();
    let mut discriminant = variant.discriminant();
    show_simple_enum_combo_box(&mut discriminant, label, ui);
    if discriminant != original_discriminant {
        state.insert(original_discriminant, variant.clone());
        *variant = state
            .get(&discriminant)
            .cloned()
            .unwrap_or_else(|| discriminant.into());
    }
}

#[allow(type_alias_bounds)]
pub type EnumMemory<T: Discriminant> = std::collections::HashMap<T::DiscriminantType, T>;

pub trait Discriminant {
    type DiscriminantType: 'static + Copy + Eq + std::hash::Hash;
    fn discriminant(&self) -> Self::DiscriminantType;
}

pub trait VariantList: 'static + Sized + Copy {
    const VARIANTS: &'static [Self];
}
pub trait IntoLabel {
    fn into_label(self) -> &'static str;
}

#[macro_export]
macro_rules! setup_enum {
    ($enum_name: path, $discriminant: ident { $($variant: ident => $label: expr),*}) => {
        #[derive(PartialEq, Eq, Clone, Copy, Hash)]
        pub enum $discriminant {
            $($variant,)*
        }

        impl widgets::enum_combo_box::Discriminant for $enum_name {
            type DiscriminantType = $discriminant;
            fn discriminant(&self) -> Self::DiscriminantType {
                match self {
                    $(Self::$variant(_) => Self::DiscriminantType::$variant,)*
                }
            }
        }
        impl Into<$enum_name> for $discriminant {
            fn into(self) -> $enum_name {
                type E = $enum_name;
                match self {
                    $(Self::$variant => E::$variant(Default::default()),)*
                }
            }
        }
        use widgets::setup_simple_enum;
        setup_simple_enum!($discriminant{ $($variant => $label),*});
    };
}
#[macro_export]
macro_rules! setup_simple_enum {
    ($enum_name: ty { $($variant: ident => $label: expr),*}) => {
        impl widgets::enum_combo_box::IntoLabel for $enum_name {
            fn into_label(self) -> &'static str {
                match self {
                    $(Self::$variant => $label,)*
                }
            }
        }
        impl widgets::enum_combo_box::VariantList for $enum_name {
            const VARIANTS: &'static [Self] = &[$(Self::$variant,)*];
        }
    }
}
