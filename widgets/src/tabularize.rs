use eframe::egui;
use metastruct::MetaStruct;

pub trait Tabularize<T> {
    fn tabularize(&mut self, id: &'static str, ui: &mut egui::Ui);
}

impl<T: MetaStruct> Tabularize<T> for T {
    fn tabularize(&mut self, id: &'static str, ui: &mut egui::Ui) {
        let grid = egui::Grid::new(id)
            .striped(true)
            .spacing([40.0, 4.0])
            .min_col_width(80.0);
        grid.show(ui, |ui| {
            for f in Self::META_FIELDS {
                ui.label(f.label);
                show_widget_by_type(&f.extract_mut, self, f.unit, ui);
                ui.end_row();
            }
        });
    }
}

pub fn show_widget_by_type<T>(
    t: &metastruct::Extractor<T>,
    obj: &mut T,
    unit: &'static str,
    ui: &mut egui::Ui,
) {
    match t {
        metastruct::Extractor::F64(extract_mut) => {
            ui.add(egui::widgets::DragValue::new(extract_mut(obj)).suffix(format!(" {}", unit)));
        }
        metastruct::Extractor::Usize(extract_mut) => {
            ui.add(egui::widgets::DragValue::new(extract_mut(obj)).suffix(format!(" {}", unit)));
        }
    }
}
