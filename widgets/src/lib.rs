//! ## Error Bar / Toast Bar
//! Nothing really special here.
//!
//! <canvas id="cvs_error_bar"></canvas>
//! <script src="examples.js"></script>
//!
//! ## Tabs
//!
//! This is a refactored version of the tabs seen in the [egui demo](https://emilk.github.io/egui/index.html#demo) ([source](https://github.com/emilk/egui/blob/master/egui_demo_lib/src/wrap_app.rs#L91)).
//!
//! <canvas id="cvs_tabs"></canvas>
//!
//! ## File Dialog / Dialog
//!
//! [path_buf_field::show_path_buf_field]: currently a simple imput text field. The idea
//! is to expand this to a file picker widget like in GTK / QT.
//!
//! [file_dialog::show_dialog]: also nothing special right now. But the idea is:
//! Given an enum, implementing `Into<&str>` and [enum_combo_box::VariantList]
//! generalize to `show_dialog<E: Into<&str> + VariantList>`.
//!
//! The function will display a dialog with all the choices available in
//! the enum `E`. It will return a response with the property `.choice()`
//! returning the chosen enum variant. Also any choice will hide the dialog.
//!
//! [file_dialog::show_file_dialog]: This one displays a file picker widget, an "OK"
//! button and a "Cancel" button.
//! If one of the buttons is pressed, the dialog is hidden.
//! Also, if the OK button is pressed, the call to this function will return
//! `Some(path)` where `path` is the current dialogs path. Otherwise
//! `None` will be returned.
//!
//! <canvas id="cvs_file_dialog"></canvas>
//!
//! ## Enum representation
//!
//! <canvas id="cvs_simple_enum_combo_box"></canvas>
//!
//! [show_simple_enum_combo_box<E: VariantList + enum_combo_box::IntoLabel + PartialEq>](enum_combo_box::show_simple_enum_combo_box):
//! This represents a simple enum (variants dont contain another type).
//! Everything it does: it fills a `egui::combo_box_with_label` with the
//! enumns variants, and acts on an instance of that enum.
//! the trait [enum_combo_box::IntoLabel] is basically `Into<&str>`.
//!
//! <canvas id="cvs_enum_combo_box"></canvas>
//!
//! [show_enum_combo_box<E: Discriminant + Clone>](enum_combo_box::show_enum_combo_box):
//! This represents a complex enum (variants can contain other types).
//! The enum has to implement [enum_combo_box::Discriminant]. This trait
//! works around the fact, that `std::mem::discriminant` is not yet
//! `const` otherwise this function could be used directly and the trait
//! would not be necessary.
//! The purpose is to have a mapping from each variant of the complex enum
//! to its discriminant (i.e. the variant without the contained
//! type). Only the string representation of the discriminant will be
//! displayed in the combo box.
//! The discriminant type also must be mappable to a representative of its
//! original variant (a sort of default trait to assign to each
//! disciminant a enum variant, filled with some default data).
//!
//! Displaying the contained data is not the responsibility of this widget.
//!
//! At the moment there is only a macro [setup_enum!] which automatically
//! implements all the necessary traits for an enum to be usable with this
//! widget.
//! In the future, some derive macros could take over this part.
//!
//! ## Representation of structs containing numeric data
//!
//! <canvas id="cvs_tabularize"></canvas>
//!
//! Main work: [metastruct] / `metastruct_derive` crates.
//! This crates define a trait [metastruct::MetaStruct] including derive macro.
//! The trait does nothing else than to attach an associated const list of
//! the numerical fields together with name, description, physical unit
//! and a symbol.
//! The derive macro automatically generates this list by parsing the doc
//! strings.
//!
//! Example: The above is auto generated from this
//!
//! ```rust
//! #[derive(Default, MetaStruct)]
//! struct Item {
//!     /// a: Field A [kg/m^3]
//!     pub a: f64,
//!     /// b: Field B [kg]
//!     pub b: f64,
//!     /// c: Field C [m]
//!     pub c: f64,
//!}
//! ```
//!
//! The trait [tabularize::Tabularize] trait is automatically implemented for structs implementing [metastruct::MetaStruct] and implements the function [tabularize::Tabularize::tabularize].
//! It is used like this: `tab.tabularize("some_id", ui)`, where `tab: Item` and `ui: egui::Ui`.
//!
//! ## Widget Array
//!
//! This represents a collection of some composite type.
//!
//! <canvas id="cvs_widget_array"></canvas>
//!
//! ## Tabular Widget Array
//!
//! Similar to [widget_array] but in tabular form.
//! Also makes use of [metastruct::MetaStruct].
//!
//! <canvas id="cvs_tabular_widget_array"></canvas>
//!
//! ## Ideas
//!
//! * For the rustdoc, autogenerate the wasm boilerplate code with
//!   something like here: https://github.com/mersinvald/aquamarine
//! * Once [#78835](https://github.com/rust-lang/rust/issues/78835) is stable, we could use `#[doc = include_str!("some_doc.md"))]`
//! <script>
//!   wasm_bindgen("./examples_bg.wasm")
//!     .then(on_wasm_loaded)["catch"](console.error);
//!   function on_wasm_loaded() {
//!     const canvas_elements = document.getElementsByTagName("canvas");
//!     for (let e of canvas_elements) {
//!       wasm_bindgen.start(e.id);
//!     }
//!   }
//! </script>

pub mod enum_combo_box;
pub mod error_bar;
pub mod file_dialog;
pub mod path_buf_field;
pub mod tabs;
pub mod tabularize;
pub mod widget_array;
