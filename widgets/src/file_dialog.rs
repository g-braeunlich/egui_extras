use crate::path_buf_field::{dir_part_exists, file_exists, show_path_buf_field, PathBufFieldState};
use eframe::egui;

pub struct FileDialogState {
    pub path_buf_field_state: PathBufFieldState,
    pub path: std::path::PathBuf,
    pub mode: FileDialogMode,
    pub open: bool,
}
impl Default for FileDialogState {
    fn default() -> Self {
        Self {
            path: Default::default(),
            path_buf_field_state: Default::default(),
            mode: Default::default(),
            open: false,
        }
    }
}
#[derive(Clone, Copy)]
pub enum FileDialogMode {
    Read,
    Write,
}
impl Default for FileDialogMode {
    fn default() -> Self {
        Self::Read
    }
}
impl Into<&str> for FileDialogMode {
    fn into(self) -> &'static str {
        match self {
            Self::Read => "Load",
            Self::Write => "Save",
        }
    }
}
impl FileDialogMode {
    pub fn validator(self) -> fn(&std::path::PathBuf) -> bool {
        match self {
            Self::Read => file_exists,
            Self::Write => dir_part_exists,
        }
    }
}

pub fn show_file_dialog(
    state: &mut FileDialogState,
    ui: &mut egui::Ui,
) -> Option<std::path::PathBuf> {
    if state.open {
        if let Some(response) = show_dialog(state.mode.into(), ui, |ui| {
            ui.heading("File:");
            show_path_buf_field(
                &mut state.path,
                &mut state.path_buf_field_state,
                state.mode.validator(),
                ui,
            );
        }) {
            state.open = false;
            if let DialogResponse::OK = response {
                return Some(state.path.clone());
            }
        }
    }
    None
}

pub enum DialogResponse {
    OK,
    Cancel,
}

pub fn show_dialog(
    ok_label: &str,
    ui: &mut egui::Ui,
    add_contents: impl FnOnce(&mut egui::Ui),
) -> Option<DialogResponse> {
    let mut response = None;
    egui::menu::bar(ui, |ui| {
        add_contents(ui);
        if ui.button(ok_label).clicked() {
            response = Some(DialogResponse::OK)
        }
        if ui.button("Cancel").clicked() {
            response = Some(DialogResponse::Cancel)
        }
    });
    response
}
