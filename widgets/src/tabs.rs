use eframe::egui;

pub type ContentCallback<T> = fn(state: &mut T, ui: &mut egui::Ui);

pub type Tab<T> = (&'static str, ContentCallback<T>);

pub trait ShowTabs<S> {
    fn show(self, selected_idx: &mut usize, state: &mut S, ui: &mut egui::Ui);
}

impl<S> ShowTabs<S> for &[Tab<S>] {
    fn show(self, selected_idx: &mut usize, state: &mut S, ui: &mut egui::Ui) {
        ui.horizontal_wrapped(|ui| {
            show_tabs(self.iter().map(|p| p.0), selected_idx, ui);
        });
        if let Some((_, show_panel)) = self.get(*selected_idx) {
            show_panel(state, ui);
        }
    }
}

pub fn show_tabs<S: Into<String> + std::fmt::Display, T: Iterator<Item = S>>(
    tabs: T,
    selected_idx: &mut usize,
    ui: &mut egui::Ui,
) {
    for (idx, label) in tabs.enumerate() {
        if ui.selectable_label(*selected_idx == idx, label).clicked() {
            *selected_idx = idx;
        }
    }
}
