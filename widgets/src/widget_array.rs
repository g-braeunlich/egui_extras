use crate::tabs::show_tabs;
use crate::tabularize::show_widget_by_type;
use eframe::egui;

pub struct WidgetArray<F> {
    show_widget: F,
}
impl<F> WidgetArray<F> {
    pub fn new(show_widget: F) -> Self {
        Self { show_widget }
    }
    pub fn show<T: Default>(
        &mut self,
        selected_widget_idx: &mut usize,
        arr: &mut Vec<T>,
        ui: &mut egui::Ui,
    ) where
        F: Fn(&mut T, &mut egui::Ui),
    {
        ui.horizontal_wrapped(|ui| {
            show_tabs(
                (0..arr.len()).map(|i| format!("# {}", i + 1)),
                selected_widget_idx,
                ui,
            );
            if ui.selectable_label(false, "➕").clicked() {
                arr.push(Default::default());
            }
            if !arr.is_empty() && ui.selectable_label(false, "🗑").clicked() {
                arr.remove(*selected_widget_idx);
                if arr.is_empty() {
                    *selected_widget_idx = 0;
                } else if *selected_widget_idx >= arr.len() {
                    *selected_widget_idx = arr.len() - 1;
                }
            }
        });
        if let Some(item) = arr.get_mut(*selected_widget_idx) {
            (self.show_widget)(item, ui);
        }
    }
}

pub struct TabularWidgetArray<T: 'static> {
    pub id: &'static str,
    pub fields: metastruct::MetaFields<T>,
}

impl<T: Default> TabularWidgetArray<T> {
    pub fn show(&self, arr: &mut Vec<T>, ui: &mut egui::Ui) {
        let grid = egui::Grid::new(self.id)
            .striped(true)
            .spacing([40.0, 4.0])
            .min_col_width(80.0);
        grid.show(ui, |ui| {
            for f in self.fields {
                ui.label(f.label);
            }
            ui.label("");
            ui.end_row();
            let mut remove_idx = None;
            for (idx, item) in arr.iter_mut().enumerate() {
                for f in self.fields {
                    show_widget_by_type(&f.extract_mut, item, f.unit, ui);
                }
                if ui.selectable_label(false, "🗑").clicked() {
                    remove_idx = Some(idx);
                }
                ui.end_row();
            }
            if ui.selectable_label(false, "➕").clicked() {
                arr.push(Default::default());
            }

            if let Some(idx) = remove_idx {
                arr.remove(idx);
            }
            ui.end_row();
        });
    }
}
