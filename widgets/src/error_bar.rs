use eframe::egui;

pub fn show_error_bar(err_msg: &mut Option<String>, ui: &mut egui::Ui) {
    let mut close_err_msg = false;
    if let Some(msg) = err_msg.as_ref() {
        egui::menu::bar(ui, |ui| {
            egui::containers::Frame::group(&egui::style::Style::default())
                .fill(CLR_ERR)
                .show(ui, |ui| {
                    ui.colored_label(egui::Color32::WHITE, msg);
                    if ui.button("❌").clicked() {
                        close_err_msg = true;
                    }
                });
        });
    }
    if close_err_msg {
        *err_msg = None;
    }
}
pub const CLR_ERR: egui::Color32 = egui::Color32::from_rgb(243, 89, 47);
